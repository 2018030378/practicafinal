package com.example.agenciadeautos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.agenciadeautos.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;


public class MainActivity extends AppCompatActivity {
    private EditText txtUsario;
    private EditText txtContrasena;
    private Button btnIngresar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUsario = findViewById(R.id.txtUsuario);
        txtContrasena = findViewById(R.id.txtContraseña);
        btnIngresar = findViewById(R.id.btnIngresa);
        btnSalir = findViewById(R.id.btnSalir);



        btnIngresar.setOnClickListener(view -> {
            //Probar en el cel del Joc
            /*Intent listProducts = new Intent(MainActivity.this, ListActivity.class);
            startActivity(listProducts);*/

            if (!txtUsario.getText().toString().isEmpty() && !txtContrasena.getText().toString().isEmpty()) {

                FirebaseAuth.getInstance().signInWithEmailAndPassword(txtUsario.getText().toString(), txtContrasena.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Datos correctos", Toast.LENGTH_SHORT).show();
                            Intent listProducts = new Intent(MainActivity.this, ListActivity.class);
                            startActivity(listProducts);
                        } else {
                            Toast.makeText(MainActivity.this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }else{
                Toast.makeText(MainActivity.this, "Favor de ingresar los datos correspondientes", Toast.LENGTH_SHORT).show();
            }
        });
        btnSalir.setOnClickListener(view -> finish());
    }
}
package com.example.agenciadeautos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.agenciadeautos.R;
import com.example.agenciadeautos.modelos.Item;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;





public class ListActivity extends AppCompatActivity
{
    RecyclerView recyclerViewItems;
    AutosAdapter autosAdapter;

    FirebaseFirestore firestore;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        recyclerViewItems = findViewById(R.id.recyclerView);
        recyclerViewItems.setLayoutManager(new LinearLayoutManager(this));
        firestore = FirebaseFirestore.getInstance();

        Query query = firestore.collection("Autos");

        //Obtiene todos los registros
        FirestoreRecyclerOptions<Item> firestoreRecyclerOptions = new FirestoreRecyclerOptions.Builder<Item>()
                .setQuery(query, Item.class)
                .build();

        //----- Set the adapter to recyclerView -------
        autosAdapter = new AutosAdapter(firestoreRecyclerOptions, this, this);
        recyclerViewItems.setAdapter(autosAdapter);
    }



    //---------- inflate menu ---------
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }



    //---------- set functionality ---------
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.option2)
        {
            Intent newProduct = new Intent(ListActivity.this, NewItemActivity.class);
            startActivityForResult(newProduct, 0);
        }
        return true;
    }



    @Override
    protected void onStart()
    {
        super.onStart();

        autosAdapter.startListening();
        autosAdapter.notifyDataSetChanged();
    }



    //----- If the user isnt in the app, stop listening for updates ------
    @Override
    protected void onStop()
    {
        super.onStop();
        autosAdapter.stopListening();
    }
}